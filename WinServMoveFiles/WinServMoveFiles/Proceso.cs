﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinServMoveFiles.Util;

using System.IO;

namespace WinServMoveFiles
{
    public class Proceso
    {
        /*private static string sourcePath0 = @"C:\gzuniga\Carpeta0";
        private static string sourcePath1 = @"C:\gzuniga\Carpeta1";
        private static string sourcePath2 = @"C:\gzuniga\Carpeta2";*/

        //Repositorio central Amadeus
        private static string sourcePath0 = @"C:\SharedFTP\amadeus\datafeed";
        //Repositorio central files procesados
        private static string sourcePath1 = @"C:\SharedFTP\amadeus\datafeed\procesados";
        //Repositorio para procesar instance Search
        private static string sourcePath2 = @"C:\_DATAFEED\Repositorio\AmadeusIS\filesRepositorio";

        private static int cantitdadTotal =  40;
        public int inicioProceso()
        {
            //Validar files en repositorio y traer files
            int files = System.IO.Directory.GetFiles(sourcePath2).Length;

            if (files <= cantitdadTotal)
            { 
                //traer files de amadeus
                int cant = cantitdadTotal - files;

                //copiar Archivos de Amadeus a Repositorio de procesamiento y mover a directorio procesados
                obtenerFilesAmadeus(cant);
            }
            
            return 0;
        }

        public static void obtenerFilesAmadeus(int cantidad)
        {
            Ln_Global.WriteToFile("##---> INI - obtenerFilesAmadeus ##" + DateTime.Now);

            string fileName = "";
            string sourcePath = sourcePath0;
            string targetPath = sourcePath2;
            string targetPathMove = sourcePath1;

            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destFile = System.IO.Path.Combine(targetPath, fileName);
            string destFileMove = System.IO.Path.Combine(targetPathMove, fileName);

            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            if (System.IO.Directory.Exists(sourcePath))
            {

                string[] files = System.IO.Directory.GetFiles(sourcePath);

                // Copia los archivos y sobreescribe archivos del destino si ya existen.
                int contador = 0;
                foreach (string s in files)
                {
                    // Use static Path methods to extract only the file name from the path.
                    /*FileInfo fi = new FileInfo(s);
                    DateTime dt = fi.LastWriteTime;*/

                    fileName = System.IO.Path.GetFileName(s);
                    destFile = System.IO.Path.Combine(targetPath, fileName);
                    destFileMove = System.IO.Path.Combine(targetPathMove, fileName);

                    string mp = "mp";
                    bool bo = fileName.Contains(mp);

                    if (bo != true)
                    {
                        if (cantidad == contador)
                            break;

                        String extFile = System.IO.Path.GetExtension(fileName);

                        if (extFile.Equals(".gz"))
                        {

                            System.IO.File.Copy(s, destFile, true);
                            System.IO.File.Move(s, destFileMove);

                        }

                        contador++;
                    }
                    else {
                        System.IO.File.Move(s, destFileMove);
                    }
                }

            }
            else
            {
                Ln_Global.WriteToFile("##---> Pasar_Files - El path de origen no existe! ##" + DateTime.Now);
                Console.WriteLine("El path de origen no existe!");
            }
            Ln_Global.WriteToFile("##---> FIN - Pasar_Files ##" + DateTime.Now);
        }

        private String extraerValor(String cadena, String stringInicial, String stringFinal)
        {
            //ejemplo : String s = 2018-12-18-18-04-16-MMPNMV_20181218180352.2.20-0-1.mr.gz
            // extraerValor(s,".", "-")
            // se obtiene valor entre (.-) = 20
            int terminaString = cadena.LastIndexOf(stringFinal);
            String nuevoString = cadena.Substring(0, terminaString);
            int offset = stringInicial.Length;
            int iniciaString = nuevoString.LastIndexOf(stringInicial) + offset;
            int cortar = nuevoString.Length - iniciaString;
            nuevoString = nuevoString.Substring(iniciaString, cortar);
            return nuevoString;
        }
    }
}
