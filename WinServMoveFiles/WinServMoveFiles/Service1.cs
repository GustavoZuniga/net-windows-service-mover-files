﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

using System.IO;
//Leer archivo
using System.Collections;
// Para añladir el tiempo
using System.Timers;

//Para comprimir y descomprimir archivos
using System.IO.Compression;

//para la conexion a la BBDD
using System.Runtime.Serialization;
using System.Data.SqlClient;
//using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System.Globalization;

using WinServMoveFiles.Util;

namespace WinServMoveFiles
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer();
        Proceso proceso = new Proceso();
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            System.Diagnostics.Debugger.Launch();
            Ln_Global.WriteToFile("El servicio se inicia en " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 300000; //numero en milisegundos (1 minuto = 60000) 
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            Ln_Global.WriteToFile("El servicio se detiene en " + DateTime.Now);
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            Ln_Global.WriteToFile("El servicio se recall en " + DateTime.Now);

            proceso.inicioProceso();

        }
    }
}
